importScripts('/src/js/idb.js')
importScripts('/src/js/utility.js')

const CACHE_STATIC_NAME = 'static-v1'
const CACHE_DYNAMIC_NAME = 'dynamic-v1'
const STATIC_FILES = [
  '/',
  '/src/js/idb.js',
  '/index.html',
  '/offline.html',
  '/src/js/app.js',
  '/src/js/feed.js',
  '/src/js/promise.js',
  '/src/js/fetch.js',
  '/src/js/material.min.js',
  'src/css/app.css',
  'src/css/feed.css',
  'src/images/main-image.jpg',
  'https://fonts.googleapis.com/css?family=Roboto:400,700',
  'https://fonts.googleapis.com/icon?family=Material+Icons',
  'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css',
]

self.addEventListener('install', function (event) {
  console.log('[Service Worker] Installing Service Worker ...', event)
  event.waitUntil(
    caches
      .open(CACHE_STATIC_NAME) // any name you like
      .then((cache) => {
        cache.addAll(STATIC_FILES)
      })
  )
})

self.addEventListener('activate', function (event) {
  console.log('[Service Worker] Activating Service Worker ....', event)
  event.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(
        keyList.map((key) => {
          if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
            console.log('[Service Worker] Removing old cache...', key)
            return caches.delete(key)
          }
        })
      )
    })
  )
  return self.clients.claim()
})

function isInArray(string, array) {
  var cachePath
  if (string.indexOf(self.origin) === 0) {
    // request targets domain where we serve the page from (i.e. NOT a CDN)
    cachePath = string.substring(self.origin.length) // take the part of the URL AFTER the domain (e.g. after localhost:8080)
  } else {
    cachePath = string // store the full request (for CDNs)
  }
  return array.indexOf(cachePath) > -1
}

self.addEventListener('fetch', function (event) {
  const url = 'https://pwagram-67bd3-default-rtdb.firebaseio.com/posts.json'
  if (event.request.url.indexOf(url) > -1) {
    event.respondWith(
      fetch(event.request).then((resp) => {
        const clonedRes = resp.clone()
        clearAllData('posts')
          .then(() => {
            return clonedRes.json()
          })
          .then((data) => {
            for (const key in data) {
              writeData('posts', data[key])
            }
          })
        return resp
      })
    )
  } else if (isInArray(event.request.url, STATIC_FILES)) {
    event.respondWith(caches.match(event.request))
  } else {
    event.respondWith(
      caches.match(event.request).then((response) => {
        if (response) {
          return response
        } else {
          return fetch(event.request)
            .then((res) => {
              return caches
                .open(CACHE_DYNAMIC_NAME) // Again, any name
                .then((cache) => {
                  cache.put(event.request.url, res.clone())
                  return res
                })
            })
            .catch(() => {
              return caches.open(CACHE_STATIC_NAME).then((cache) => {
                if (event.request.headers.get('accept').includes('text/html')) {
                  // if incoming request accepts HTML as an answer
                  return cache.match('/offline.html')
                }
              })
            })
        }
      })
    )
  }
})

self.addEventListener('sync', (evt) => {
  console.log('[SW:] Background syncing', evt)
  if (evt.tag === 'sync-new-post') {
    console.log('[SW:] Syncing new Post')
    evt.waitUntil(
      readAllData('sync-posts').then((data) => {
        for (const dt of data) {
          fetch('https://pwagram-67bd3-default-rtdb.firebaseio.com/posts.json', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
            body: JSON.stringify({
              id: dt.id,
              title: dt.title,
              location: dt.location,
              image:
                'https://firebasestorage.googleapis.com/v0/b/pwagram-67bd3.appspot.com/o/sf-boat.jpg?alt=media&token=94da8e0d-40f8-4b5a-9baa-1574358e4ea8',
            }),
          })
            .then((res) => {
              console.log('Sent data', res)
              if (res.ok) {
                deleteItemFromData('sync-posts', dt.id)
              }
            })
            .catch((err) => {
              console.log('Error while sending data', err)
            })
        }
      })
    )
  }
})
