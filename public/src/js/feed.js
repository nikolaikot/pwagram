var shareImageButton = document.querySelector('#share-image-button')
var createPostArea = document.querySelector('#create-post')
var closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn')
var sharedMomentsArea = document.querySelector('#shared-moments')
const form = document.querySelector('form')
const titleInput = document.querySelector('#title')
const locationInput = document.querySelector('#location')

function openCreatePostModal() {
  createPostArea.style.display = 'block'
  if (deferredPrompt) {
    deferredPrompt.prompt()

    deferredPrompt.userChoice.then(function (choiceResult) {
      console.log(choiceResult.outcome)

      if (choiceResult.outcome === 'dismissed') {
        console.log('User cancelled installation')
      } else {
        console.log('User added to home screen')
      }
    })

    deferredPrompt = null
  }
}

function closeCreatePostModal() {
  createPostArea.style.display = 'none'
}

shareImageButton.addEventListener('click', openCreatePostModal)

closeCreatePostModalButton.addEventListener('click', closeCreatePostModal)

function clearCards() {
  while (sharedMomentsArea.hasChildNodes()) {
    sharedMomentsArea.removeChild(sharedMomentsArea.lastChild)
  }
}

function createCard(data) {
  var cardWrapper = document.createElement('div')
  cardWrapper.className = 'shared-moment-card mdl-card mdl-shadow--2dp'
  var cardTitle = document.createElement('div')
  cardTitle.className = 'mdl-card__title'
  cardTitle.style.backgroundImage = `url(${data.image})`
  cardTitle.style.backgroundSize = 'cover'
  cardTitle.style.height = '180px'
  cardWrapper.appendChild(cardTitle)
  var cardTitleTextElement = document.createElement('h2')
  cardTitleTextElement.className = 'mdl-card__title-text'
  cardTitleTextElement.textContent = data.title
  cardTitle.appendChild(cardTitleTextElement)
  var cardSupportingText = document.createElement('div')
  cardSupportingText.className = 'mdl-card__supporting-text'
  cardSupportingText.textContent = data.location
  cardSupportingText.style.textAlign = 'center'
  cardWrapper.appendChild(cardSupportingText)
  componentHandler.upgradeElement(cardWrapper)
  sharedMomentsArea.appendChild(cardWrapper)
}

function updateUi(data) {
  clearCards()
  for (let i = 0; i < data.length; i++) {
    createCard(data[i])
  }
}

function transformDataToArray(data) {
  const dataArray = []
  for (const key in data) {
    dataArray.push(data[key])
  }
  return dataArray
}

const url = 'https://pwagram-67bd3-default-rtdb.firebaseio.com/posts.json'
let networkDataReceived = false

fetch(url)
  .then(function (res) {
    return res.json()
  })
  .then(function (data) {
    networkDataReceived = true
    console.log('From Web', data)
    updateUi(transformDataToArray(data))
  })

if ('indexedDB' in window) {
  readAllData('posts').then((data) => {
    if (!networkDataReceived) {
      console.log('From cache', data)
      updateUi(data)
    }
  })
}

function sendData() {
  fetch('https://pwagram-67bd3-default-rtdb.firebaseio.com/posts.json', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({
      id: new Date().toISOString(),
      title: titleInput.value.trim(),
      location: locationInput.value.trim(),
      image:
        'https://firebasestorage.googleapis.com/v0/b/pwagram-67bd3.appspot.com/o/sf-boat.jpg?alt=media&token=94da8e0d-40f8-4b5a-9baa-1574358e4ea8',
    }),
  }).then((res) => {
    console.log('Sent data', res)
  })
}

form.addEventListener('submit', (evt) => {
  evt.preventDefault()

  if (titleInput.value.trim() === '' || locationInput.value.trim() === '') {
    alert('Please enter valid data!')
    return
  }

  closeCreatePostModal()

  if ('serviceWorker' in navigator && 'SyncManager' in window) {
    navigator.serviceWorker.ready.then((sw) => {
      const post = {
        id: new Date().toISOString(),
        title: titleInput.value.trim(),
        location: locationInput.value.trim(),
      }
      writeData('sync-posts', post)
        .then(() => {
          return sw.sync.register('sync-new-post')
        })
        .then(() => {
          const snackbarContainer = document.querySelector('#confirmation-toast')
          const data = { message: 'Your Post was saved for syncing!' }
          snackbarContainer.MaterialSnackbar.showSnackbar(data)
        })
        .catch((err) => {
          console.log(err)
        })
    })
  } else {
    sendData()
  }
})
